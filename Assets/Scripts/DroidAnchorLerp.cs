﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroidAnchorLerp : MonoBehaviour
{
	private Vector2 oldPos;
	private Vector2 newPos;

	private void Start()
	{
		oldPos = transform.position;
	}

	private void FixedUpdate()
	{
		if (newPos != (Vector2)transform.position)
			newPos = transform.position;

		transform.position = Vector2.Lerp(oldPos, newPos, 2f * Time.fixedDeltaTime);
	}
}
