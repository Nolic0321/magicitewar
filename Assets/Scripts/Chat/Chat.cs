﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
public class Chat : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI chatMessages;
	[SerializeField] protected TMP_InputField inputField;
	protected string player;
	public void Init(string playerName)
	{
		player = playerName;
	}
	internal void AddMessage(string message)
	{
		chatMessages.text += "\n" + message;
	}

	public virtual void SendMessage(string player, string message)
	{
		Debug.LogError("Send Message Base method should never be called...but it has :(");
	}


}