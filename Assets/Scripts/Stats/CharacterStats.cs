﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class CharacterStats : NetworkBehaviour
{

    [SyncVar]
    public int MaxHealth;
    [SyncVar]
    private bool _isDead = false;
    [SerializeField]
    [SyncVar(hook = "ChangingHealth")]
    protected int _currentHealth;

    public int CurrentHealth { get { return _currentHealth; } }

    [Header("Optional: ")]
    [SyncVar]
    public Stat Damage;

    [Client]
    protected virtual void VerifyServerHealthValue(int fromServer)
    {
        //Stubbed to be overwritten by child classes
    }

    private void ChangingHealth(int fromServer)
    {
        //Server is ALWAYS correct
        _currentHealth = fromServer;
        VerifyServerHealthValue(fromServer);
    }

    [Command]
    protected void CmdUpdateHealthOnServer(int health)
    {
        _currentHealth = health;
    }

    private void Awake()
    {
        _currentHealth = MaxHealth;
    }


    public virtual void Setup()
    {
        //Stub method for now
    }

    [Client]
    public virtual void TakeDamage(int dmg)
    {
        if (_isDead)
        {
            Debug.LogErrorFormat("{0} should be dead but isn't", name);
            return;
        }

        dmg = Mathf.Clamp(dmg, 0, int.MaxValue);
        _currentHealth = Mathf.Clamp(_currentHealth - dmg, 0, int.MaxValue);
        if (isLocalPlayer)
            CmdUpdateHealthOnServer(_currentHealth);
        if (_currentHealth <= 0)
        {
            Die();
        }
    }
    [Command]
    public void CmdAttack(GameObject target)
    {
        Attack(target.GetComponent<CharacterStats>());
    }


    public void Attack(CharacterStats stats)
    {
        stats.TakeDamage(Damage.GetValue());
    }


    public virtual void Die()
    {
        _isDead = true;
        AmDead(_isDead);
    }

    private void AmDead(bool dead)
    {
        if (!dead)
            Debug.LogErrorFormat("{0} is calling server-side dead but isDead = {1}", name, dead);
        _isDead = dead;
        NetworkServer.Destroy(gameObject);
    }
}
