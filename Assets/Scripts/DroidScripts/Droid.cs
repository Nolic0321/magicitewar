﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Droid : NetworkBehaviour
{
	private Stat woodCuttingStat;
	private Stat plantGatheringStat;
	private Stat oreMiningStat;
	private Stat expStat;
	public PlayerStats player;

	public void Initialize()
	{
		woodCuttingStat = new Stat();
		plantGatheringStat = new Stat();
		oreMiningStat = new Stat();
		expStat = new Stat();
	}

	protected virtual void ActivateAbility()
	{
		Debug.Log(name + " activated ability!");
	}

	protected virtual void Harvest(Vector2 pos)
	{
		Debug.LogFormat("{0} is going to {1} to harvest",name,pos);
	}

	




}
