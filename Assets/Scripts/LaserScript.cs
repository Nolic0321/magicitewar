﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

	public Transform target;
	public Color laserColor = Color.red;
	[Range(0,10)]
	public float waitTime = 3f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator RandomShot(){
		yield return new WaitForSeconds(waitTime);
		//Fire
		yield return null;
	}
}
