﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PrefabManager : MonoBehaviour
{
    protected PrefabManager() { }

    // Assign the prefab in the inspector
    public GameObject[] EnemyPrefabs;

    private void Start()
    {
        SpawnEnemy();
    }

    public void SpawnEnemy(Vector2 pos)
    {
        GameObject obj = Instantiate(EnemyPrefabs[Random.Range(0, EnemyPrefabs.Length)], pos, Quaternion.identity);
        obj.GetComponent<EnemyStats>().prefabManager = this;
        NetworkServer.Spawn(obj);
    }

    public void SpawnEnemy()
    {
        SpawnEnemy(transform.position);
    }
}