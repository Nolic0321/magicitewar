﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

[RequireComponent(typeof(CustomNetworkManager))]
public class ConnectionManagerScript : MonoBehaviour
{
    [SerializeField] private CustomNetworkManager _manager;

    private void Start()
    {
        StartMatchMaker();
    }

    public void StartMatchMaker()
    {
        if (_manager.matchMaker == null)
            _manager.StartMatchMaker();
    }

    public void StartSinglePlayer()
    {
        _manager.StartHost(new ConnectionConfig(), 1);
    }

    public void StartLan()
    {
        _manager.StartHost();
    }

    public void JoinLan()
    {
        _manager.StartClient();
    }

    public void CreateInternetMatch(string matchName)
    {
        if (!matchName.Equals(""))
            _manager.matchName = matchName;
        _manager.matchMaker.CreateMatch(_manager.matchName, _manager.matchSize, true, "", "", "", 0, 0, _manager.OnMatchCreate);
    }

    public void Disconnect()
    {
        bool noConnection = (_manager.client == null || _manager.client.connection == null ||
                                 _manager.client.connection.connectionId == -1);
        if (!_manager.IsClientConnected() && !NetworkServer.active && _manager.matchMaker == null)
        {
            Debug.Log("Stopping Client");
            _manager.StopClient();
        }

        if (NetworkServer.active || _manager.IsClientConnected())
        {
            Debug.Log("Stopping Host");
            _manager.StopHost();
            NetworkServer.Reset();
        }

    }
}

