﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class CustomNetworkManager : NetworkManager
{
    [SerializeField] private bool _usMatchMaking = true;
    [SerializeField] private bool _euMatchMaking = false;
    [SerializeField] private GameObject _customPlayerPrefab;
    private void Awake()
    {
        if (_usMatchMaking)
            SetMatchHost("us1-mm.unet.unity3d.com", matchPort, true);
        if (_euMatchMaking)
            SetMatchHost("eu1-mm.unet.unity3d.com", matchPort, true);
    }
}
