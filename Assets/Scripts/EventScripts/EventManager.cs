﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour
{
    private static EventManager eventManager;

    private Dictionary<string, UnityEvent> eventDictionary;
    private Dictionary<string, StringUnityEvent> oneStringParamDictionary;

    public static EventManager instance
    {
        get
        {
            if (!eventManager)
                eventManager = FindObjectOfType<EventManager>();

            if (!eventManager)
                Debug.LogError("There needs to be one active EventManager script on a GameObject in your scene");
            else
                eventManager.Init();

            return eventManager;
        }
    }

    private void Init()
    {
        if (eventDictionary == null)
            eventDictionary = new Dictionary<string, UnityEvent>();
        if (oneStringParamDictionary == null)
            oneStringParamDictionary = new Dictionary<string, StringUnityEvent>();
        DontDestroyOnLoad(gameObject);
    }

    public static void StartListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent = null;

        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction listener)
    {
        if (eventManager == null) return;

        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent)) thisEvent.RemoveListener(listener);
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent = null;

        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent)) thisEvent.Invoke();
    }

    public static void StartListening(string eventName, UnityAction<string> listener)
    {
        StringUnityEvent thisEvent = null;

        if (instance.oneStringParamDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new StringUnityEvent();
            thisEvent.AddListener(listener);
            instance.oneStringParamDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<string> listener)
    {
        if (eventManager == null) return;

        StringUnityEvent thisEvent = null;
        if (instance.oneStringParamDictionary.TryGetValue(eventName, out thisEvent)) thisEvent.RemoveListener(listener);
    }

    public static void TriggerStringEvent(string eventName, string input)
    {
        StringUnityEvent stringEvent = null;

        if (instance.oneStringParamDictionary.TryGetValue(eventName, out stringEvent)) stringEvent.Invoke(input);
    }
}

public class StringUnityEvent : UnityEvent<string>
{

}