﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Equipment
{
    public GameObject EquipablePrefab;
    public Stat Damage;
    public bool Onehanded = false;
    public bool Twohanded = false;

    public void Init(string n, string d, Sprite i, EquipmentSlot e, GameObject ep, Stat dg, bool h, bool hh)
    {
        Init(n, d, i, e);
        EquipablePrefab = ep;
        Damage = dg;
        Onehanded = h;
        Twohanded = hh;
    }
    public override void Init(string n, string d, Sprite i, EquipmentSlot e)
    {
        base.Init(n, d, i, e);
    }
}
