﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName ="Audio Events/Simple")]
public class SimpleAudioEvent : AudioEvent {

    public AudioClip[] clips;

    [MinMaxSlider(0,100)]
    public Vector2 volume;

    [MinMaxSlider(0,2)]
    public Vector2 pitch;

    public override void Play(AudioSource source)
    {
        if(clips.Length ==0) return;
        source.clip = clips[Random.Range(0,clips.Length)];
        source.volume = Random.Range(volume.x,volume.y);
        source.pitch = Random.Range(pitch.x,pitch.y);
        source.Play();
    }

    

    
}

[System.Serializable]
public struct RangedFloat
{
	public float minValue;
	public float maxValue;
}