﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : Item {
    public EquipmentSlot EquipSlot;

    public virtual void Init(string n, string d, Sprite i, EquipmentSlot e)
    {
        Init(n, d, i);
        EquipSlot = e;
    }
    public override void Init(string n, string d, Sprite i)
    {
        base.Init(n, d, i);
    }
    public override void Use()
    {
        base.Use();
    }
}


public enum EquipmentSlot { Head, Chest, MainHand, OffHand}