﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Droplist")]
public class DropList : ScriptableObject {
    public Item[] List;
}
