﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class HUDScript : MonoBehaviour
{
    [SerializeField] private Slider _hpSlider;
    [SerializeField] private TextMeshProUGUI _hpText;
    [SerializeField] private Slider _stamSlider;
    [SerializeField] private TextMeshProUGUI _stamText;

    private PlayerStats playerStats;

    // Use this for initialization
    private void Start()
    {
        GameObject.DontDestroyOnLoad(gameObject);
    }

    public void Init(PlayerStats stats)
    {
        playerStats = stats;
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateHealth();
    }

    public void UpdateHealth()
    {
        if (playerStats)
            UpdateStat(playerStats.CurrentHealth, playerStats.MaxHealth, _hpSlider, _hpText);
    }

    public void UpdateStamina()
    {
        //Stub
    }

    private void UpdateStat(int current, int max, Slider slider, TextMeshProUGUI text)
    {
        slider.maxValue = max;
        slider.value = current;
        text.text = string.Format("{0}/{1}", current, max);
    }
}
