﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;
using TMPro;

public class JoinButton : MonoBehaviour {

    public Button Button;
    public TextMeshProUGUI MatchName;

    private NetworkManager _manager;
    private MatchInfoSnapshot _match;

    public void Start()
    {
        Button.onClick.AddListener(JoinMatch);
    }

    public void Setup(MatchInfoSnapshot match, NetworkManager manager)
    {
        this._manager = manager;
        this._match = match;
        this.MatchName.SetText(match.name);
    }

    public void JoinMatch()
    {
        _manager.matchName = _match.name;
        _manager.matchMaker.JoinMatch(_match.networkId, "", "", "", 0, 0, _manager.OnMatchJoined);
    }
}
