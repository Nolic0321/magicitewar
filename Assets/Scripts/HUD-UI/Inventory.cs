﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Inventory : MonoBehaviour
{
    protected Inventory() { }

    public delegate void OnItemChanged();
    public OnItemChanged OnItemChangedCallback;
    public Item[] Items;

    [HideInInspector] public int Space = 30;


    public void Init()
    {
        Items = new Item[Space];
    }

    public bool Add(Item item, int id)
    {
        if (Items[id] != null)
            return false;

        Items[id] = item;
        InvokeItemChangedCallback();
        return true;
    }

    private void InvokeItemChangedCallback()
    {
        if (OnItemChangedCallback != null)
            OnItemChangedCallback.Invoke();
    }

    public bool Add(Item item)
    {
        for (int i = 0; i < Items.Length; i++)
        {
            if (Add(item, i))
                return true;
        }
        return false;
    }

    public void SwapById(int slot1, int slot2)
    {
        Item temp = Items[slot1];
        Items[slot1] = Items[slot2];
        Items[slot2] = temp;
        InvokeItemChangedCallback();

    }

    internal void Remove(int id)
    {
        UnityEngine.Assertions.Assert.IsNotNull(Items[id]);
        Items[id] = null;

        InvokeItemChangedCallback();
    }

    public void Remove(Item item)
    {
        for (int i = 0; i < Items.Length; i++)
        {
            if (Items[i] == item)
                Items[i] = null;
        }

        InvokeItemChangedCallback();
    }

    public void Clear()
    {
        for (int i = 0; i < Items.Length; i++)
        {
            Items[i] = null;
        }
    }

}
