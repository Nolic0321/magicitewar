﻿using UnityEngine;
using UnityEngine.UI;   
using System.Collections;

/**
 * Scale a GridLayoutGroup according to resolution, etc.
 * This is using width-constrained layout
 */
public class GridScalar : MonoBehaviour
{

    private GridLayoutGroup _grid;
    private RectOffset _gridPadding;
    private RectTransform _parent;

    public int Rows = 6;
    public int Cols = 7;
    public float Spacing = 10;

    private Vector2 _lastSize;

    private void Start()
    {
        _grid = GetComponent<GridLayoutGroup>();
        _grid.spacing = new Vector2(Spacing, Spacing);
        _parent = GetComponent<RectTransform>();
        _gridPadding = _grid.padding;
        _lastSize = Vector2.zero;
    }

    // Update is called once per frame
    private void Update()
    {
        if (_lastSize == _parent.rect.size)
        {
            return;
        }

        var paddingX = _gridPadding.left + _gridPadding.right;
        var cellSize = Mathf.Round((_parent.rect.width - paddingX - (Rows - 1) * Spacing) / Rows);
        _grid.cellSize = new Vector2(cellSize, cellSize);
    }
}