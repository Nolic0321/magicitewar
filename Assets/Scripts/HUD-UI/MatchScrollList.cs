﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class MatchScrollList : MonoBehaviour
{
    [SerializeField] private NetworkManager _manager;
    [SerializeField] private GameObject _buttonPrefab;
    [SerializeField] private Transform _contentPanel;

    private void Start()
    {
        _manager = GameObject.Find("NetworkManager").GetComponent<CustomNetworkManager>();
        GetMatchList();
    }

    private void GetMatchList()
    {
        _manager.GetComponent<ConnectionManagerScript>().StartMatchMaker();
        _manager.matchMaker.ListMatches(0, 20, "", false, 0, 0, SetMatchList);
    }

    private void SetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        _manager.matches = matchList;
        PopulateMatchList();
    }

    public void RefreshList()
    {
        DestroyList();
        GetMatchList();
    }

    private void DestroyList()
    {
        for (int i = _contentPanel.childCount - 1; i >= 0; i--)
        {
            Destroy(_contentPanel.GetChild(i).gameObject);
        }
    }

    public void PopulateMatchList()
    {

        for (int i = 0; i < _manager.matches.Count; i++)
        {
            var match = _manager.matches[i];

            AddButton(match);
        }
    }
    public void AddButton(MatchInfoSnapshot match)
    {

        GameObject newButton = Instantiate(_buttonPrefab, _contentPanel);
        newButton.GetComponent<JoinButton>().Setup(match, _manager);
    }
}
