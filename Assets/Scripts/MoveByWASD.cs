﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByWASD : MonoBehaviour
{

    public int speed = 5;

    private Vector3 newPos;

    private void Start()
    {
        newPos = transform.position;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A)){
            newPos = transform.position + Vector3.left * speed;
        }

        if (Input.GetKey(KeyCode.D)){

            newPos = transform.position + Vector3.right * speed;
        }
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, newPos, speed * Time.fixedDeltaTime);
    }
}
