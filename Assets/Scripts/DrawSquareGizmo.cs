﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSquareGizmo : MonoBehaviour {

	
	public Color gizmoColor;
	public Vector3 size;
	// Use this for initialization
	private void OnDrawGizmos()
	{
		Gizmos.color = gizmoColor;
		Gizmos.DrawWireCube(transform.position,size);
	}
}
