﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class EnemyStats : CharacterStats
{

    [SerializeField]
    private Slider _slider;
    [SerializeField] private GameObject _dropItemPrefab;
    [HideInInspector]public PrefabManager prefabManager;
    public override void Setup()
    {
        base.Setup();
        _slider = GetComponentInChildren<Slider>();
        _slider.maxValue = MaxHealth;
        UpdateHealthBar();
    }
    public override void OnStartClient()
    {
        Setup();
    }

    protected override void VerifyServerHealthValue(int fromServer)
    {
        if (_currentHealth != fromServer) {
            Debug.LogWarningFormat("{0}'s _currentHealth ({1}) did not match value from server ({2}", name, _currentHealth, fromServer);
            _currentHealth = fromServer;
        }
        UpdateHealthBar();
    }
    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        if (_slider)
            _slider.value = _currentHealth;
    }

    public override void Die()
    {
        UnityEngine.Assertions.Assert.IsTrue(isServer);
        RpcDropItem();
        prefabManager.SpawnEnemy();

    }

    [ClientRpc]
    private void RpcDropItem()
    {
        Instantiate(_dropItemPrefab, transform.position, Quaternion.identity);
        base.Die();
    }
}
