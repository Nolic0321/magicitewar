﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] private float _fallMultiplier = 3.5f;
    [SerializeField] private int _speed = 1;
    [SerializeField] private int _decisionTime = 3;
    [SerializeField] private Transform _bodyContainer;
    private Rigidbody2D _rb;
    private Vector2 _moveDirection;
    private Animator _anim;
    public bool IsAttacking = false;

    private Coroutine _patrol;
    // Use this for initialization
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        _patrol = StartCoroutine(Patrol());
    }

    private IEnumerator Patrol()
    {
        while (true)
        {
            ChangeMind();
            yield return new WaitForSeconds(UnityEngine.Random.Range(0, _decisionTime));

        }
    }

    private void FixedUpdate()
    {
        _rb.velocity += Vector2.up * Physics2D.gravity.y * (_fallMultiplier - 1) * Time.deltaTime;
        _rb.AddForce(_moveDirection, ForceMode2D.Impulse);
    }

    private void ChangeMind()
    {
        switch (UnityEngine.Random.Range(1, 4))
        {
            case 1:
                WalkLeft();
                break;
            case 2:
                WalkRight();
                break;
            case 3:
                Idle();
                break;
            default:
                Idle();
                break;
        }
    }

    private void WalkLeft()
    {
        _moveDirection = Vector2.left * _speed;
        //_bodyContainer.SetPositionAndRotation(transform.position, Quaternion.Euler(0, 180, 0));
        _bodyContainer.rotation = Quaternion.Euler(0,180,0);
        WalkAnimation(true);
    }

    private void WalkRight()
    {
        _moveDirection = Vector2.right * _speed;
        //_bodyContainer.SetPositionAndRotation(transform.position, Quaternion.Euler(0, 0, 0));
        _bodyContainer.rotation = Quaternion.identity;
        WalkAnimation(true);
    }

    private void Idle()
    {
        _moveDirection = Vector2.zero;
        WalkAnimation(false);
    }

    private void WalkAnimation(bool isWalking)
    {
        if (_anim.GetBool("isWalking") == isWalking)
            return;
        _anim.SetBool("isWalking", isWalking);
    }
}
