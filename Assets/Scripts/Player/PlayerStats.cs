﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using System;

public class PlayerStats : CharacterStats
{
    [SerializeField] private TextMeshProUGUI _tmp;
    [SerializeField] private Animator _anim;
    public HUDScript Hud;

    public override void Setup()
    {
        base.Setup();
    }

    public override void OnStartLocalPlayer()
    {
        Setup();
        UpdateHealthStatusText();
        CmdUpdateHealthOnServer(_currentHealth);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        //If a Player already connected we need to update OUR instance of TMP to show their current health
        //According to Unity Docs _currentHealth should already be SyncVar'd by now...
        if (isClient && !isServer)
        {
            UpdateHealthStatusText();
        }
    }

    public void Respawn()
    {
        _currentHealth = MaxHealth;
        CmdUpdateHealthOnServer(_currentHealth);
        _anim.SetBool("isDead", false);
    }

    [Client]
    private void UpdateHealthStatusText()
    {
        if (_tmp)
            _tmp.SetText("{0}/{1}", _currentHealth, MaxHealth);
    }

    [Client]
    protected override void VerifyServerHealthValue(int fromServer)
    {
        if (_currentHealth != fromServer)
        {
            Debug.LogWarningFormat("{0}'s _currentHealth ({1}) did not match value from server ({2}", name, _currentHealth, fromServer);
            _currentHealth = fromServer;
        }
        UpdateHealthStatusText();
    }

    public override void Die()
    {
        _anim.SetBool("isDead",true);
        CmdDie();
        GetComponent<PlayerInput>().enabled = false;
        Hud.GetComponent<DeathMenu>().ToggleMenu(true);
    }

    [Command]
    private void CmdDie()
    {
        RpcSetTrigger();
    }

    [ClientRpc]
    private void RpcSetTrigger()
    {
        if (!isLocalPlayer)
            _anim.SetTrigger("death");
    }

    [Client]
    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        UpdateHealthStatusText();
        StartCoroutine(HitCycle());
    }

    private IEnumerator HitCycle()
    {
        _anim.SetBool("invincible", true);
        gameObject.layer = LayerMask.NameToLayer("PlayerInvincible");
        Coroutine pulse = StartCoroutine(PulseSprites());
        yield return new WaitForSeconds(1f);
        gameObject.layer = LayerMask.NameToLayer("Player");
        _anim.SetBool("invincible", false);
        StopCoroutine(pulse);
        SetSpriteAlpha(1);
        yield return null;
    }

    private void SetSpriteAlpha(int alpha)
    {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
        for(int i = 0; i < sprites.Length; i++)
        {
            sprites[i].color = new Color(1, 1, 1, alpha);
        }
    }

    private IEnumerator PulseSprites()
    {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
        while(true)
        {
            SetSpriteAlpha(0);
            yield return new WaitForFixedUpdate();
            yield return new WaitForSeconds(.1f);
            SetSpriteAlpha(1);
            yield return new WaitForFixedUpdate();
            yield return new WaitForSeconds(.1f);
        }
    }
}
