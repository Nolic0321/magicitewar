﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthTextUiScript : MonoBehaviour {
    [SerializeField] private Transform _target;
    [SerializeField] private float _aboveTarget = 1f;
    private RectTransform _rect;

    private void Start()
    {
        _rect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    private void Update () {
        Vector3 pos = Camera.main.WorldToScreenPoint(_target.position);
        int screenHeight = Camera.main.pixelHeight;
        _rect.position = pos + Vector3.up * _aboveTarget;
	}
}
