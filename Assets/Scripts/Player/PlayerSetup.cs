﻿using TMPro;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkIdentity))]
public class PlayerSetup : NetworkBehaviour {
    [SerializeField] private Behaviour[] _otherPlayerComponentsToDisable;
    [SerializeField] private Behaviour[] _playerComponentsToDisable;
    [SerializeField] private GameObject _hudPrefab;
    [SerializeField] private GameObject _chatPrefab;
    [SerializeField] private TextMeshProUGUI _nameTag;
    [SerializeField] private GameObject[] droidPrefabs;
    [SerializeField] private bool spawnDroidsOnPlayerSpawn = true;
    [SyncVar] private string _playerName;
    private Animator _anim;
    private CharacterStats _stats;
    private GameObject _hud;

    private GameManager _manager;
    // Use this for initialization

    private void Awake() {
        _manager = GameObject.Find("GameManager").GetComponent<GameManager>();

    }

    private void Start() {

        if (!isLocalPlayer) {
            DisableOtherClientComponents();
        }

        if (isClient && isLocalPlayer) {
            GameObject.Find("PlayerCam").GetComponent<Cinemachine.CinemachineVirtualCamera>().Follow = gameObject.transform;
            _manager.PlayerTransform = transform;
            DisableComponents();
            SetupHud();

        }
    }

    [Command]
    private void CmdSpawnDroids(GameObject player) {
        for (int i = 0; i < droidPrefabs.Length; i++) {
            GameObject droid = Instantiate(droidPrefabs[i], null);
            droid.GetComponent<DroidMovement>().player = player.transform;
            NetworkServer.SpawnWithClientAuthority(droid, connectionToClient);
            TargetStartDroid(connectionToClient, droid);

        }
    }

    [TargetRpc]
    void TargetStartDroid(NetworkConnection playerConnection, GameObject droid) {
        DroidMovement droidMove = droid.GetComponent<DroidMovement>();
        PlayerStats player = GetComponent<PlayerStats>();
        droid.GetComponent<Droid>().player = player;
        droidMove.player = transform;
        StartCoroutine(droidMove.MakeDecision());
    }

    public override void OnStartLocalPlayer() {
        if (spawnDroidsOnPlayerSpawn)
            CmdSpawnDroids(gameObject);
        _playerName = "Player " + GetComponent<NetworkIdentity>().netId;
        name = _playerName;
        _nameTag.text = _playerName;
        CmdUpdateName(name);
    }

    public override void OnStartClient() {
        name = _playerName;
        _nameTag.text = name;
    }

    void OnDestroy() {
        if (_manager)
            _manager.GetComponent<Inventory>().Clear();
        GameObject.Destroy(_hud);
    }

    [Command]
    private void CmdUpdateName(string newName) {
        name = newName; //Don't NEED this here but adding it just in case :)
        RpcSetNewName(newName);
    }

    [ClientRpc]
    private void RpcSetNewName(string newName) {
        name = newName;
        _nameTag.text = newName;
    }

    private void SetupHud() {
        if (isLocalPlayer) {
            _hud = Instantiate(_hudPrefab);
            _hud.GetComponent<HUDScript>().Init(GetComponent<PlayerStats>());
            GetComponent<PlayerStats>().Hud = _hud.GetComponent<HUDScript>();

            _hud.GetComponent<DeathMenu>().Init(gameObject);

            var chat = _hud.GetComponent<UNetChat>();
            chat.Init(_playerName);

            var pauseMenu = _hud.GetComponent<PauseMenu>();
            var inventoryMenu = _hud.GetComponent<InventoryMenu>();
            GetComponent<PlayerInput>().Init(chat, pauseMenu, inventoryMenu);
        }
    }

    private void DisableComponents() {
        foreach (Behaviour component in _playerComponentsToDisable) {
            component.enabled = false;
        }
    }
    private void DisableOtherClientComponents() {
        foreach (Behaviour component in _otherPlayerComponentsToDisable) {
            component.enabled = false;
        }
    }

    [Command]
    void CmdUserSentMessage(string message) {
        RpcServerHasNewChatMessage(message);
    }

    [ClientRpc]
    void RpcServerHasNewChatMessage(string message) {
        EventManager.TriggerStringEvent("ServerMessage", message);
    }
}