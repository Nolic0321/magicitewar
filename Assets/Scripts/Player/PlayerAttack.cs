﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    [SerializeField]
    private SwitchHitBoxScript _hitBox;

    // Update is called once per frame
    public void Attack()
    {
        _hitBox.IsAttacking = true;
        StartCoroutine(StopAttack());
    }

    IEnumerator StopAttack()
    {
        yield return new WaitForSecondsRealtime(.5f);
        _hitBox.IsAttacking = false;
        yield return null;
    }
}
