﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSwapper : MonoBehaviour {

    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _singlePlayerMenu;
    [SerializeField] private GameObject _multiplayerMenu;
    [SerializeField] private GameObject _optionsMenu;
    // Use this for initialization

    public void ToggleMainMenu()
    {
        DeactivateAllMenus();
        _mainMenu.SetActive(true);
    }

    public void ToggleSingleplayerMenu()
    {
        DeactivateAllMenus();
        _singlePlayerMenu.SetActive(true);
    }

    public void ToggleMultiplayerMenu()
    {
        DeactivateAllMenus();
        _multiplayerMenu.SetActive(true);
    }

    public void ToggleOptionsMenu()
    {
        DeactivateAllMenus();
        _optionsMenu.SetActive(true);
    }

    private void DeactivateAllMenus()
    {
        _mainMenu.SetActive(false);
        //singlePlayerMenu.SetActive(false);
        _multiplayerMenu.SetActive(false);
        //optionsMenu.SetActive(false);
    }
}
