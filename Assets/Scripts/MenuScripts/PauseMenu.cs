﻿// This file is auto-generated. Do not modify or move this file.

using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour, IMenu {

    [SerializeField] private GameObject _pausePanel;
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _optionsButton;
    [SerializeField] private Button _quitButton;
    private ConnectionManagerScript _connectionManager;

    private void Start() {
        _connectionManager = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<ConnectionManagerScript>();
        SetupButtons();
    }

    private void SetupButtons() {
        _resumeButton.onClick.AddListener(Resume);
        _optionsButton.onClick.AddListener(Options);
        _quitButton.onClick.AddListener(Quit);
    }
    private void Options() {
        throw new NotImplementedException();
    }

    public bool IsActive() {
        return _pausePanel.activeSelf;
    }
    public void ToggleMenu() {
        _pausePanel.SetActive(!_pausePanel.activeSelf);
    }

    public void Resume() {
        _pausePanel.SetActive(false);
    }

    public void Quit() {
        _pausePanel.SetActive(false);
        if (_connectionManager)
            _connectionManager.Disconnect();
    }
}