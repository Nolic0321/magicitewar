﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;

public class MultiplayerMenu : MonoBehaviour
{

    [SerializeField] private Button _createInternetMatch;
    [SerializeField] private Button _createLanMatch;
    [SerializeField] private Button _joinLanMatch;
    [SerializeField] private Button _refreshList;
    [SerializeField] private Button _mainMenu;
    [SerializeField] private TextMeshProUGUI _internetMatchName;
    [SerializeField] private MatchScrollList _matchList;
    [SerializeField] private ConnectionManagerScript _connectionManager;
    [SerializeField] private GameObject _networkManagerPrefab;
    [SerializeField] private GameObject _mainMenuPanel;
    [SerializeField] private GameObject _multiplayerPanel;
    [SerializeField] private MenuSwapper _menuSwapper;

    private void Start()
    {
        if (!_connectionManager)
            _connectionManager = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<ConnectionManagerScript>();
        SetupButtons();
    }

    private void SetupButtons()
    {
        _createInternetMatch.onClick.AddListener(CreateInternetMatch);
        _createLanMatch.onClick.AddListener(CreateLanMatch);
        _joinLanMatch.onClick.AddListener(JoinLanMatch);
        _refreshList.onClick.AddListener(RefreshExistingGamesList);
        _mainMenu.onClick.AddListener(ToggleMainMenu);
    }

    private void ToggleMainMenu()
    {
        _menuSwapper.ToggleMainMenu();
    }

    private void RefreshExistingGamesList()
    {
        _matchList.RefreshList();
    }

    private void JoinLanMatch()
    {
        _connectionManager.JoinLan();
    }

    private void CreateLanMatch()
    {
        _connectionManager.StartLan();
    }

    private void CreateInternetMatch()
    {
        _connectionManager.CreateInternetMatch(_internetMatchName.text);
    }
}
