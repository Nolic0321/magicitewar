﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
public class DeathMenu : MonoBehaviour, IMenu
{
    [SerializeField] private GameObject _deathPanel;
    [SerializeField] private Button _respawnButton;
    [SerializeField] private Button _quitButton;
    private ConnectionManagerScript _connectionManager;

    private NetworkStartPosition[] _spawnPoints;
    private GameObject Player;

    private void Start()
    {
        _connectionManager = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<ConnectionManagerScript>();
        _spawnPoints = FindObjectsOfType<NetworkStartPosition>();
        SetupButtons();
    }

    public void Init(GameObject playerObject)
    {
        Player = playerObject;
    }

    private void SetupButtons()
    {
        _respawnButton.onClick.AddListener(Respawn);
        _quitButton.onClick.AddListener(Quit);
    }
    public void Respawn()
    {
        PlayerStats playerStats = Player.GetComponent<PlayerStats>();
        playerStats.Respawn();
        Player.transform.position = _spawnPoints[Random.Range(0, _spawnPoints.Length)].transform.position;
        Player.GetComponent<PlayerInput>().enabled = true;
        ToggleMenu(false);

    }

    public void Quit()
    {
        ToggleMenu(false);
        GameObject.Destroy(GameObject.FindGameObjectWithTag("HUD"));
        _connectionManager.Disconnect();
    }

    internal void ToggleMenu(bool active)
    {
        _deathPanel.SetActive(active);
    }

    public void ToggleMenu(){
        _deathPanel.SetActive(!_deathPanel.activeSelf);
    }

    public bool IsActive(){
        return _deathPanel.activeSelf;
    }
}
