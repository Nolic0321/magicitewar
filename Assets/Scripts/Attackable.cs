﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attackable : MonoBehaviour
{
    [TagSelector]
    public List<string> EnemyTags = new List<string>();

    public bool IsATarget(string tag)
    {
        return EnemyTags.Contains(tag);
    }

    public List<string> GetMyEnemies()
    {
        return EnemyTags;
    }
}
